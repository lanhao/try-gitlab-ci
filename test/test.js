const moment = require('moment');
const { assert } = require('chai');

describe("moment test", () => {
  it("should return year", () => {
    assert.equal(moment().format("YYYY"), (new Date()).getFullYear());
  });
});
